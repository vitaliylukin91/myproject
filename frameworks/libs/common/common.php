<?php
/**
 * Сокращенные аннотации & стандарты: http://blog.brolib.ru/articles/22.html;
 *
 * -A, U- Артем Андреевич Балобанов,
 ** http://workshop.brolib.com/masters/id-1.html,
 ** http://www.ains.pro (http://blog.brolib.com/master/id-1.html);
 *
 * -Date, Time- 2016.01.07 15:40;
 * -D- Программный продукт: BroGallery;
*/
class common {
	public $sitepath = NULL ;
	public function __construct() {
		$this->sitepath = dirname(__FILE__). '/../../..';
	}
	/**
	 * -D, Method- Запись в лог файл;
	 * -V- {String} @message: Сообщение;
	 * -R- {boolean} ;
	*/
	public function log($message = ''){
		
		$log = date('Y.m.d H:i:s').":\t". $message."\n---\n";
		return (!file_put_contents($this->sitepath.'/logs/common.log', $log, FILE_APPEND|LOCK_EX) != true);
	}
}
?>