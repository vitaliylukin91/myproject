<?php
/**
 * Сокращенные аннотации & стандарты: http://blog.brolib.ru/articles/19.html ;
 *
 * -A, U- Артем Андреевич Балобанов,
 ** http://workshop.brolib.com/masters/id-1.html,
 ** http://www.ains.pro (http://blog.brolib.com/master/id-1.html) ;
 *
 * -Date, Time- 2015.06.19 10:00 ;
 * -D- Класс для отправки данных в Roistat CRM;
*/
class Roistat {
	public  $key = NULL,
			$result= NULL;
	private $common= NULL;
	/**
	 * -V- {String} @key: Ключ от Roistat;
	*/
	public function __construct($key = ''){
		$this->key = $key;
		
		if (!class_exists('common'))
			include_once(dirname(__FILE__). '/../common/common.php');
		
		$this->common = new common();
		
		define('ROISTAT_KEY', $this->key);
	}
	
	/**
	 * -D, Method- Отослать данные в CRM;
	 * -V- {String} @leadName: название ;
	 * -V- {String} @leadDesc: описание ;
	 * -D, V- {Array} @params: параметры (см. вх. параметры функции @sendLeadToRoistat);
	 * -R- {String};
	*/
	public function send(
		$leadName= '', 
		$leadDesc= '',
		$params= array()
	){
		
		$defPar = array('email', 'phone', 'name', 'params');
		
		$p = '';
		foreach ($defPar as $p) {
			if (isset($params[$p]) === false)
				$params[$p] = '';
		}
		
		if ($params['params'] == '' || is_array($params['params']) == false)
			$params['params']= array();
		
		if (function_exists('sendLeadToRoistat')) {
			$this->result = sendLeadToRoistat(
				$leadName,
				$leadDesc,
				$params['email'],
				$params['phone'],
				$params['name'],
				$params['params']
			);
			
			$this->common->log('Roistat send log. Params: leadName: '.$leadName.' & leadDesc: '.$leadDesc. ' & params: '.print_r($params, true).'. Result: '.$this->result);
			
		} else {
			$this->result = 'Function @sendLeadToRoistat not exists!';
			$this->common->log('Roistat send log. '.$this->result);
		}
		
		return $this->getResult();
		
	}
	/**
	 * -Method-;
	 * -R- {String};
	*/
	public function getResult(){
		
		return $this->result ;
	}
}
?>