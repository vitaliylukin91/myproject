jQuery(function($){

    $("#nav-scroll a").on('click', function(){
        $('.section-' + $(this).attr('data-section')).animatescroll();
    });

    $('.catalog-category').on('click', function(){
        $('#category-' + $(this).attr('data-section')).animatescroll();
    });
    $('.scrolling-catalog').on('click', function(){
        $('#catalog').animatescroll();
    });

    $('.scrolling-company').on('click', function(e){
        e.preventDefault();
        $('.video-company').animatescroll();
    });
    $('.scrolling-guaranty').on('click', function(e){
        e.preventDefault();
        $('.section-11').animatescroll();
    });

    function quickCategorySelect() {
        var windowPos = $(window).scrollTop();
        $('.catalog-category').each(function () {
            var $this = $(this);
            var categoryItem = $('#category-' + $this.attr("data-section"));
            if ((categoryItem.offset().top - 100) <= windowPos && categoryItem.offset().top + categoryItem.innerHeight() > windowPos) {
                $('.catalog-category').removeClass('active');
                $this.addClass('active');
            }
        });
    }
    function quickLinkSelect() {
        var windowPos = $(window).scrollTop();
        $('#nav-scroll a').each(function () {
            var $this = $(this);
            var sectionItem = $('.section-' + $this.attr("data-section"));
            if ((sectionItem.offset().top - 100) <= windowPos && sectionItem.offset().top + sectionItem.innerHeight() > windowPos) {
                $('#nav-scroll a').removeClass('active');
                $this.addClass('active');
            }
        });
    }

    $(window).scroll(function(){
        var positionWindow = $(window).scrollTop();
        var section = $('.section-2');
        var categoriesBlock = $('.catalog-categories');
        var positionTop = section.offset().top + 100;
        var positionBottom = positionTop + parseInt(section.outerHeight()) - parseInt(categoriesBlock.outerHeight());
        if(positionWindow >= positionTop && positionWindow <= positionBottom){
            categoriesBlock.addClass('fixed');
        } else {
            categoriesBlock.removeClass('fixed');
        }
        quickCategorySelect();
        quickLinkSelect();
    });

    //video
    $('#play-button').on('click', function(){
        $(this).hide();
        $('#video-frame').show();
    });
    $('#play-button-small').on('click', function(){
        $(this).hide();
        $('#video-frame-small').show();
    });
    $('#play-button-2').on('click', function(){
        $(this).hide();
        $(this).next().hide();
        $('#video-frame-2').show();
    });

    //form order in catalog
    $('.catalog-products a').on('click', function(e){
        e.preventDefault();
        var nameKitchen = $(this).text();
        $('#form-kitchen-name').text(nameKitchen);
        $('#pwebcontact98_toggler').click();
        $('#pwebcontact98_field-subject').val(nameKitchen);
    });
    $('.catalog-products .jshop_img').on('click', function(){
        var nameKitchen = $(this).attr('data-name-product');
        $('#form-kitchen-name').text(nameKitchen);
        $('#pwebcontact98_toggler').click();
        $('#pwebcontact98_field-subject').val(nameKitchen);
    });

    $('.send-order-design').on('click', function(){
        $('#pwebcontact99_toggler').click();
    });

    $('.send-order-plan').on('click', function(){
        $('#pwebcontact100_toggler').click();
    });
    $('.send-order-size').on('click', function(){
        $('#pwebcontact112_toggler').click();
    });
    $('.callback-link').on('click', function(){
        $('#pwebcontact113_toggler').click();
    });

    //input masc
    $("input[type='tel']").inputmask("+9 (999) 999-99-99");

    //thanks page
    if($('.thanks-page').length){
        $('.thanks-page').css('height', $(window).outerHeight());
    }

});
