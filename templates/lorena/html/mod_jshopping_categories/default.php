<div class="catalog-categories">
    <?php foreach($categories_arr as $k => $curr): ?>
        <div class="catalog-category <?php if(!$k) echo 'active' ?>" data-section="<?php print $curr->category_id; ?>">
            <?php if ($show_image && $curr->category_image){?>
                <img align = "absmiddle" src = "<?php print $jshopConfig->image_category_live_path."/".$curr->category_image?>" alt = "<?php print $curr->name?>" />
            <?php } ?>
            <span class="catalog-category-name"><?php print $curr->name; ?></span><br>
            <span class="catalog-category-desc"><?php print $curr->short_description; ?></span>
        </div>
    <?php endforeach; ?>
</div>
