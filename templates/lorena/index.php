<?php
defined('_JEXEC') or die;

$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$doc->addStyleSheet('templates/'.$this->template.'/css/template.css');

JHtml::_('jquery.framework');
$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/animatescroll.js', 'text/javascript');
$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/jquery.inputmask.js', 'text/javascript');
$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/main.js', 'text/javascript');

$middleBlock = false;
if ($this->countModules('section-2-2')||$this->countModules('section-3')||$this->countModules('section-4')||$this->countModules('section-5')||$this->countModules('section-6')||$this->countModules('section-7')||$this->countModules('section-8')||$this->countModules('section-9')||$this->countModules('section-10')||$this->countModules('section-11')||$this->countModules('section-12')||$this->countModules('callback')||$this->countModules('map')){
    $middleBlock = true;
}
?>

<!DOCTYPE html>
<html>
<head>    
    <jdoc:include type="head" />
    <?php JHTML::_('behavior.modal'); ?>
    <meta name="viewport" content="width=device-width; initial-scale=1.0">
    <meta name="MobileOptimized" content="320"/>
    <meta name="HandheldFriendly" content="true"/>
</head>
<body>
<?php if ($this->countModules('header')) : ?>
    <header id="header" class="section-1">
        <jdoc:include type="modules" name="header" style="no" />
    </header>
<?php endif; ?>
<?php if ($this->countModules('banner')) : ?>
    <section id="main-banner">
        <jdoc:include type="modules" name="banner" style="no" />
    </section>
<?php endif; ?>
<jdoc:include type="modules" name="nav-scroll" style="no" />
<jdoc:include type="modules" name="thanks" style="no" />
<?php if ($this->countModules('section-2')) : ?>
    <section id="catalog" class="section-2">
        <jdoc:include type="modules" name="section-2" style="no" />
        <div class="clr"></div>
    </section>
<?php endif; ?>
<?php if ($middleBlock) : ?>
    <div class="middle-block">
        <jdoc:include type="message" />
        <jdoc:include type="component" />
        <jdoc:include type="modules" name="section-2-2" style="no" />
        <section class="section-3">
            <jdoc:include type="modules" name="section-3" style="no" />
        </section>
        <section class="section-4">
            <jdoc:include type="modules" name="section-4" style="no" />
        </section>
        <section class="section-5">
            <jdoc:include type="modules" name="section-5" style="no" />
        </section>
        <section class="section-6">
            <jdoc:include type="modules" name="section-6" style="no" />
        </section>
        <section class="section-7">
            <jdoc:include type="modules" name="section-7" style="no" />
        </section>
        <section class="section-8">
            <jdoc:include type="modules" name="section-8" style="no" />
        </section>
        <section class="section-9">
            <jdoc:include type="modules" name="section-9" style="no" />
        </section>
        <section class="section-10">
            <jdoc:include type="modules" name="section-10" style="no" />
        </section>
        <section class="section-11">
            <jdoc:include type="modules" name="section-11" style="no" />
        </section>
        <section class="section-12">
            <jdoc:include type="modules" name="section-12" style="no" />
            <div class="row">
                <div class="col-2 callback">
                    <jdoc:include type="modules" name="callback" style="no" />
                </div>
                <div class="col-2">
                    <jdoc:include type="modules" name="map" style="no" />
                </div>
            </div>
        </section>
    </div>
<?php endif; ?>
<?php if ($this->countModules('copyright')) : ?>
    <footer id="copyright">
        <jdoc:include type="modules" name="copyright" style="no" />
    </footer>
<?php endif; ?>
<script>
(function(w, d, s, h, id) {
    w.roistatProjectId = id; w.roistatHost = h;
    var p = d.location.protocol == "https:" ? "https://" : "http://";
    var u = /^.*roistat_visit=[^;]+(.*)?$/.test(d.cookie) ? "/dist/module.js" : "/api/site/1.0/"+id+"/init";
    var js = d.createElement(s); js.async = 1; js.src = p+h+u; var js2 = d.getElementsByTagName(s)[0]; js2.parentNode.insertBefore(js, js2);
})(window, document, 'script', 'cloud.roistat.com', '9474');
</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter34622635 = new Ya.Metrika({
                    id:34622635,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/34622635" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-72233989-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- /end Google Analytics -->

<!-- vk remarketing -->
<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=hEWhm/U1zC0ELAnmgr3BySEJn8MJk*wiN3BhTuK1IYN350UkR*zdO7XLB/ZoWnJEcGm3Kd6c7EGX0SrvqxrkzSGnqkrLtxbtDepdJahmfx3GZkLSdGYzcYw*di6N2iBIZW542UC4DRxwL1*t1/zF//ckR9/5m3fgUlhyXNfgxcw-';</script>
<!-- /end vk remarketing -->

<!-- Rating@Mail.ru counter -->
<script type="text/javascript">
var _tmr = window._tmr || (window._tmr = []);
_tmr.push({id: "2731646", type: "pageView", start: (new Date()).getTime()});
(function (d, w, id) {
  if (d.getElementById(id)) return;
  var ts = d.createElement("script"); ts.type = "text/javascript"; ts.async = true; ts.id = id;
  ts.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//top-fwz1.mail.ru/js/code.js";
  var f = function () {var s = d.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ts, s);};
  if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); }
})(document, window, "topmailru-code");
</script><noscript><div style="position:absolute;left:-10000px;">
<img src="//top-fwz1.mail.ru/counter?id=2731646;js=na" style="border:0;" height="1" width="1" alt="�������@Mail.ru" />
</div></noscript>
<!-- //Rating@Mail.ru counter -->


<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','//connect.facebook.net/en_US/fbevents.js');

fbq('init', '454192954787161');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=454192954787161&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code --></body>
</html>
